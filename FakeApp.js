import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { Pseudodashboard } from './Pseudodashboard';
import { Pseudodabout } from './Pseudoabout';
import { Pseudologin } from './login';
import './App.css';

/*
-- Not a working example. Just contains pseudocode for protecting routes.
    -- Any components that would like be protected should be specified within the <Route path='/dashboard' element={<Dashboard />} /> tag
    -- Any components that do not need to be protected should be outside the <Route element={<PrivateRoute />}> tag
-- Instead of setting browser router in this file, it is defined in index.js. This seem to help with blank pages when trying to redirect
*/

function FakeApp() {
    // For testing: Setting auth session variable to true.
    window.sessionStorage.setItem("auth", true)

    return (
        <main className="App">
        <Routes>
            <Route element={<PrivateRoute />}>
                <Route path='/dashboard' element={<Pseudodashboard />} />
                <Route path='/about' element={<Pseudodabout />} />
            </Route>
            <Route path="/" element={<Pseudologin />} />
        </Routes>
        </main>
    );
}

export default FakeApp;