import React from 'react';
import { Navigate, Outlet } from "react-router-dom";

// A method to check authentication stored in session
const useAuth=() => {
    const isAuthenticated = sessionStorage.getItem('auth')
    if(isAuthenticated) {
        return true
    } else {
        return false
    }
}

// Render Component is isAthenticated set to true. Else return to '/' path
const PrivateRoute = () => {
    const isAuthenticated = useAuth();
    console.log(isAuthenticated)

    return (
        isAuthenticated ? <Outlet /> : <Navigate replace to="/" />
    )
}

export default PrivateRoute;