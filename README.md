# Pseudo Example: Route Protection react-douter-dom v6

## <ins>**Summary**</ins>

This repo contains a pseudo example for protecting routes for react. The following files are described below:

* FakeApp.js : 
    * Contains the layout of Route protection
    * Any protected routes should be within the Route tag containing the <Protected_Route> element
    * Non-protected routes should be outside this tag

* Protected_Routes.js
    * Contains an authorization function to check for the auth variable
    * Contains the logic for redirection
        * If the auth is set to true, return <Outlet /> element. The <Outlet /> is a placeholder for rendering nested routes with a specific element.
        * If false, return to specified path

* index.js
    * Contains the <BrowserRouter> element for the app

* package.json
    * Contains the package versions


